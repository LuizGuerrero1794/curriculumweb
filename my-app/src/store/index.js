import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    curriculum:{
      toolbarC: 'black',
    },
    user:{
      photo: '/img/curriculum/mini.jpg',
    }
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
